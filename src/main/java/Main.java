import org.apache.poi.ss.usermodel.Workbook;
import org.jxls.common.Context;
import org.jxls.reader.ReaderBuilder;
import org.jxls.reader.XLSReadStatus;
import org.jxls.reader.XLSReader;
import org.jxls.util.JxlsHelper;
import template.mapping.Product;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        List<Product> products = read();
        for (Product p : products) {
            p.validate(); // validate to add error
        }
        write(products);
    }

    public static List<Product> read() throws Exception {
        String xmlConfig = "product.xml";
        String dataXLS = "product.xlsx";
        java.io.InputStream stream = Main.class.getResourceAsStream(xmlConfig);
        InputStream inputXML = new BufferedInputStream(stream);
        XLSReader mainReader = ReaderBuilder.buildFromXML(inputXML);
        InputStream inputXLS = new BufferedInputStream(Main.class.getResourceAsStream(dataXLS));
        List<Product> products = new ArrayList<>();
        Map beans = new HashMap();
        beans.put("products", products);
        XLSReadStatus readStatus = mainReader.read(inputXLS, beans);
        System.out.println(products);
        return products;
    }

    public static void write(List<Product> products) throws Exception {
        try (InputStream is = Main.class.getResourceAsStream("product-error.xlsx")) {
            try (OutputStream os = new FileOutputStream("product-error-out.xlsx")) {
                Context context = new Context();
                context.putVar("products", products);
                JxlsHelper.getInstance().processTemplate(is, os, context);
            }
        }
    }
}
