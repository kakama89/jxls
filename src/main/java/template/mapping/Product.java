package template.mapping;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import template.Validate;

@Getter
@Setter
@ToString
public class Product implements Validate {
    private String orderName;

    private String businessName;

    private String customsName;

    private String regulatoryName;

    private String ps;

    private String unit;

    private String partNo;

    private String error;

    @Override
    public void validate() {
        if (this.error == null) {
            this.error = StringUtils.EMPTY;
        }

        StringBuilder builder = new StringBuilder(this.error);
        if (StringUtils.isEmpty(orderName) || StringUtils.length(orderName) > 512) {
            builder.append("\nTen dat hang la bat buoc, max length la 512");
        }
        if (StringUtils.isEmpty(businessName) || StringUtils.length(businessName) > 512) {
            builder.append("\nTen kinh doanh la bat buoc, max length la 512");
        }
        if (StringUtils.isEmpty(customsName) || StringUtils.length(customsName) > 256) {
            builder.append("\nTen thong quan la bat buoc, max length la 256");
        }

        if (StringUtils.isNotEmpty(regulatoryName) && StringUtils.length(regulatoryName) > 300) {
            builder.append("\nTen thong quan max length la 300");
        }

        if (StringUtils.isEmpty(ps) || StringUtils.length(ps) > 5) {
            builder.append("\nMa dong san pham max length la 5");
        }

        if (StringUtils.isEmpty(unit)) { // todo
            builder.append("\nDon vi tinh la bat buoc");
        }


        if (StringUtils.isNotEmpty(partNo) && StringUtils.length(partNo) > 20) {
            builder.append("\npartno max length la 20");
        }
        this.error = builder.toString();
    }
}